package com.elavon.training.main;

import com.elavon.training.impl.TheCalculator;


public class Main{

	public static void main(String[] args) {
		
        TheCalculator calc = new TheCalculator();
        
        calc.getSum(10, 20);
        calc.getDifference(20, 10);
        calc.getProduct(90, 3400);
        calc.getQuotientAndRemainder(17, 5);
        calc.toCelsius(212);
        calc.toFahrenheit(100);
        calc.toKilogram(154);
        calc.toPound(53.37);
        calc.isPalindrome("level");
        calc.isPalindrome("river");
        
        
	}
}
