package com.elavon.training.impl;

import com.elavon.training.inte.AwesomeCalculator;

public class TheCalculator implements AwesomeCalculator{


	@Override
	public int getSum(int augend, int addend) {
		
		int getSum = augend + addend;
		System.out.println("The sum of " + augend + " and " + addend + " is: "+getSum);
		return getSum;	
		
	}

	@Override
	public double getDifference(double minuend, double subtrahend) {
		double getDifference = minuend - subtrahend;
		System.out.println("The difference of " + minuend + " and " + subtrahend + " is: "+getDifference);
		return getDifference;
	}

	@Override
	public double getProduct(double multiplicand, double multiplier) {
		double getProduct = multiplicand * multiplier;
		System.out.println("The product of " + multiplicand + " and " + multiplier + " is: "+getProduct);
		return getProduct;
	}

	@Override
	public int getQuotientAndRemainder(int dividend, int divisor) {
		int getQuotient = dividend/divisor;
		System.out.print("Returns " +getQuotient);
		int getRemainder = dividend % divisor; 
		System.out.println(" remainder "+getRemainder);
		return getQuotient;
	}

	@Override
	public double toCelsius(int fahrenheit) {
		
		double celsius = (0.5556)*(fahrenheit-32);
		System.out.println("Fahrenheit to Celsius = "+celsius);
		return celsius;
	}

	@Override
	public double toFahrenheit(int celsius) {
		
		double fahrenheit = (1.8) * celsius + 32;
		System.out.println("Celsius to Fahrenheit = "+fahrenheit);
		return fahrenheit;
	}

	@Override
	public double toKilogram(double lbs) {
		
		double toKilogram = lbs * 0.454;
		System.out.println("Pounds to kilogram = "+toKilogram);
		return toKilogram;
	}

	@Override
	public double toPound(double kg) {
		
		double toPound = kg * 2.204623;
		System.out.println("Kilogram to Pound = "+toPound);
		return toPound;
	}

	@Override
	public boolean isPalindrome(String str) {
		
		boolean isPalindrome = true; 
        for (int i = 0 , j = str.length() - 1 ; i < j ; i ++ , j --) { 
            if (str.charAt(i) != str.charAt(j)) { 
                isPalindrome = false; 
            } 
        } 
        System.out.println(str + " - > " + isPalindrome); 
        return isPalindrome; 
		
	}
}
