package com.elavon.jtutorial.tv;

public class Television {
	
		protected String brand;
		protected String model;
		protected boolean powerOn;
		protected int channel;
		protected int volume;
		
		
		Television(String Televisionbrand, String Televisionmodel)
		{
			this.brand = Televisionbrand;
			this.model = Televisionmodel;
			this.powerOn = false;
			this.channel = 0;
			this.volume = 5;
		}
		public static void main(String[] args)
		{
			Television tv = new Television("Andre Electronics", "ONE");
			System.out.println("Default Value: " +tv);
			
			//Turn on TV
			tv.turnOn();
			
			//Switch Channel Up 5 times
			tv.channelUp();
			tv.channelUp();
			tv.channelUp();
			tv.channelUp();
			tv.channelUp();
			
			//Switch Channel Down 1 tim
			tv.channelDown();
			
			//Switch Volume Down 3 times
			tv.volumeDown();
			tv.volumeDown();
			tv.volumeDown();
			
			//Switch Volume Up 1 time
			tv.volumeUp();
			
			//Turn off TV 
			tv.turnOff();
			
			System.out.println("New Value: " +tv);

		}	
		public String toString()
		{
			String tvInfo = brand + " " + model + " [On: " + powerOn + ", Channel: " + Integer.toString(channel) + ", Volume: " + Integer.toString(volume) + "]";
			return tvInfo;
		}
		boolean turnOn()
		{
			return powerOn = true;
		}
		boolean turnOff()
		{
			return powerOn = false;
		}
		int channelUp()
		{
			if (channel<13)
			{
				channel++;
			}
			else
			{
				channel=13;
			}
			return channel;
		}
		int channelDown()
		{
			if(channel>0)
			{
				channel--;
			}
			else
			{
				channel=0;
			}
			return channel;
		}
		int volumeUp()
		{
			if(volume<10)
			{
				volume++;
			}
			return volume;
		}
		int volumeDown()
		{
			if(volume>0)
			{
				volume--;
			}
			return volume;
		}
}
