package com.elavon.jtutorial.tv;

public class coloredTV extends Television{
	
	int brightness;
	int contrast;
	int picture;
	
	coloredTV(String Televisionbrand, String Televisionmodel) {
		super(Televisionbrand, Televisionmodel);
		this.brightness=50;
		this.contrast=50;
		this.picture=50;
	}
	
	public static void main(String[] args)
	{
		
		coloredTV tv1 = new coloredTV("Andre Electronics", "ONE");
		System.out.println(tv1);
		
		Television bnwTV = new Television("Admiral", "A1");
		coloredTV sonyTV = new coloredTV("SONY", "S1");
		
		sonyTV.brightnessUp();
		
		System.out.println(bnwTV);
		System.out.println(sonyTV);
		
		coloredTV sharpTV = new coloredTV("SHARP", "SH1");
		
		sharpTV.mute();
		
		//Adjusted properties of sharpTV
		sharpTV.brightnessUp();
		sharpTV.brightnessUp();
		sharpTV.brightnessUp();
		
		sharpTV.brightnessDown();
		
		sharpTV.contrastDown();
		
		sharpTV.contrastUp();
		sharpTV.contrastUp();
		
		sharpTV.pictureUp();
		sharpTV.pictureUp();
		sharpTV.pictureUp();
		sharpTV.pictureUp();
		
		sharpTV.pictureDown();
		
		System.out.println(sharpTV);
	}
	public String toString()
	{
		//String tvInfor = "[b:" + brightness + " c:" + contrast + " p:" + picture + "]";
		String tvInfor = brand + " " + model + " [On:" + powerOn + ", channel:" + Integer.toString(channel) + ", volume:" + Integer.toString(volume) + " ] [b:" + brightness + " c:" + contrast + " p:" + picture + "]";
		return tvInfor;	
	}
	
	int brightnessUp()
	{
		brightness++;
		return brightness;
	}
	int brightnessDown()
	{
		brightness--;
		return brightness;
	}
	int contrastUp()
	{
		contrast++;
		return contrast;
	}
	int contrastDown()
	{
		contrast--;
		return contrast;
	}
	int pictureUp()
	{
		picture++;
		return picture;
	}
	int pictureDown()
	{
		picture--;
		return picture;
	}
	int switchToChannel(int switchChannel)
	{
		return channel = switchChannel;
	}
	int mute()
	{
		return volume = 0;
	}
}
