package com.elavon.tutorial.Christmas;

public class TwelveDaysOfChristmas {

	public static void main(String[] args) {
		TwelveDaysOfChristmas.printLyrics();

	}
	static void printLyrics()
	{
		System.out.println("Twelve Days Of Christmas Lyrics");
		System.out.println();
		for(int x = 0; x <= 12; x++)
		{
			
			if(x==1)
			{
				System.out.println("On the first day of Christmas");
				System.out.println("My true love gave to me");		
			}
			else if(x==2)
			{
				System.out.println();
				System.out.println("On the second day of Christmas");
				System.out.println("My true love gave to me");
			}
			else if(x==3)
			{
				System.out.println();
				System.out.println("On the third day of Christmas");
				System.out.println("My true love gave to me");
			}
			else if(x==4)
			{
				System.out.println();
				System.out.println("On the fourth day of Christmas");
				System.out.println("My true love gave to me");
			}
			else if(x==5)
			{
				System.out.println();
				System.out.println("On the fifth day of Christmas");
				System.out.println("My true love gave to me");
			}
			else if(x==6)
			{
				System.out.println();
				System.out.println("On the sixth day of Christmas");
				System.out.println("My true love gave to me");
			}
			else if(x==7)
			{
				System.out.println();
				System.out.println("On the seventh day of Christmas");
				System.out.println("My true love gave to me");
			}
			else if(x==8)
			{
				System.out.println();
				System.out.println("On the eight day of Christmas");
				System.out.println("My true love gave to me");
			}
			else if(x==9)
			{
				System.out.println();
				System.out.println("On the ninth day of Christmas");
				System.out.println("My true love gave to me");
			}
			else if(x==10)
			{
				System.out.println();
				System.out.println("On the tenth day of Christmas");
				System.out.println("My true love gave to me");
			}
			else if(x==11)
			{
				System.out.println();
				System.out.println("On the eleventh day of Christmas");
				System.out.println("My true love gave to me");
			}
			else if(x==12)
			{
				System.out.println();
				System.out.println("On the twelfth day of Christmas");
				System.out.println("My true love gave to me");
			}
			for(int y=x; y>=1; y--)
			{
				if(y==12)
				{
					System.out.println("12 Drummers Drumming");
				}
				else if(y==11)
				{
					System.out.println("Eleven Pipers Piping");
				}
				else if(y==10)
				{
					System.out.println("Ten lords-a-leaping");
				}
				else if(y==9)
				{
					System.out.println("Nine ladies dancing");
				}
				else if(y==8)
				{
					System.out.println("Eight maids-a-milking");
				}
				else if(y==7)
				{
					System.out.println("Seven swans-a-swimming");
				}
				else if(y==6)
				{
					System.out.println("Six geese-a-laying");
				}
				else if(y==5)
				{
					System.out.println("Five golden rings");
				}
				else if(y==4)
				{
					System.out.println("Four calling birds");
				}
				else if(y==3)
				{
					System.out.println("Three French hens");
				}
				else if(y==2)
				{
					System.out.println("Two turtle doves and");
				}
				else if(y==1)
				{
					System.out.println("A partridge in a pear tree");
				}
				
			}
		}
	}

}
