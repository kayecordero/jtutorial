package com.elavon.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;

/**
 * 
 * @author KPCordero
 *
 */

public class ArrayCollectionsExercise {

	public static void main(String[] args) {
		//My Top Country to Visit
		String[] myCountry = new String[]{"South Korea","Japan","Singapore","UAE","France","Italy","Greece","USA","Spain","New Zealand"};
		System.out.println("Size of the country Array: " +myCountry.length);
		
		//Array List of the countries
		ArrayList<String> countryList = new ArrayList<String>(Arrays.asList(myCountry));
		System.out.println("Size of the country Array List: " +countryList.size());
		
		//Add Scotland
		countryList.add("Scotland");
		
		System.out.println("List of countries is empty or not: " +countryList.isEmpty());
		System.out.println("The New size of the country Array List: " +countryList.size());
		System.out.println("Is Mongolia on the list of the Country? " +countryList.contains("Mongolia"));
		System.out.println("Is Scotland on the list of the Country? "+countryList.contains("Scotland"));
		countryList.remove(10);
		
		//top 5 destinations
		System.out.println("------------------------------------");
		String[] topFiveDesti = new String[]{"Indonesia", "United Kingdom", "France", "Italy", "USA"};
		System.out.println("Top 5 destinations are:");
		for(int x= 0; x<topFiveDesti.length; x++)
		{
			System.out.println(topFiveDesti[x]+" ");
		}
		System.out.println("Is Indonesia on the list? " +countryList.contains("Indonesia"));
		System.out.println("Is United Kingdom on the list? "+countryList.contains("United Kingdom"));
		System.out.println("Is France on the list? "+countryList.contains("France"));
		System.out.println("Is Italy on the list? "+countryList.contains("Italy"));
		System.out.println("Is USA on the list? "+countryList.contains("USA"));	
		
		System.out.println("------------------------------------");
		System.out.println("The list of Countries");
		System.out.println(countryList);
		
		HashMap<Integer, String> countryMap = new HashMap<Integer, String>();
		
		countryMap.put(1,"South Korea");
		countryMap.put(2,"Japan");
		countryMap.put(3,"Singapore");
		countryMap.put(4,"UAE");
		countryMap.put(5,"France");
		countryMap.put(6,"Italy");
		countryMap.put(7,"Greece");
		countryMap.put(8,"USA");
		countryMap.put(9,"Spain");
		countryMap.put(10,"New Zealand");
		
		System.out.println("------------------------------------");
		System.out.println("Rank of countries: "+countryMap);
		
		Collections.sort(countryList);
		LinkedHashSet<String> lhs = new LinkedHashSet<String>();
		
		lhs.addAll(countryList);
		
		countryList.clear();

		// Sort countries
		countryList.addAll(lhs);
		System.out.println("------------------------------------");
		System.out.println("List of countries alphabetically : "+countryList);
		
		//rank countries
		System.out.println("------------------------------------");
		System.out.println("Most Loved countries");
		System.out.println("Rank #1: " +countryMap.get(1));
		System.out.println("Rank #2: " +countryMap.get(2));
		System.out.println("Rank #3: " +countryMap.get(5));
		System.out.println("Rank #4: " +countryMap.get(8));
		System.out.println("Rank #5: " +countryMap.get(6));
		System.out.println("Rank #6: " +countryMap.get(9));
		System.out.println("Rank #7: " +countryMap.get(10));
		System.out.println("Rank #8: " +countryMap.get(7));
		System.out.println("Rank #9: " +countryMap.get(3));
		System.out.println("Rank #10: " +countryMap.get(4));
		
		}
	}
//}
